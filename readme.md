# Video Encoding Process
ffmpeg -i track.mp3 -i video.mov -s 640x640 -vcodec h264 -shortest output.mp4

# Global

[X] Setup timing
[X] Emit an End event to Artwork
[ ] Look into Mobile stuff
[ ] Mobile remove webGL stuff
[X] Play with the web type
[ ] Preload spinner

# Launch

[ ] Cache Busting
[X] RBMA Routes
[X] Update the Social image stuff
[ ] Sponsored video post
  - Mess up the track (Anenon) for it
  - Capture a nice moment from the feedback loop
[ ] Push RBMA / Credits to footer of Calendar
[X] Fly in the RBMA logo
[ ] Load Calendar > Open Stream > Back > History
[X] Small Excerpt links

# Sound

[X] Tweak the signal chain
[ ] Seamless loop
[X] Basic error handling
[X] Optimize Preloading (Safari)

# Artwork

[X] Clear the buffer on Load
[X] Read from the Options
[X] Six frame loop that fills the frame

# Design

[ ] Disable one minute option
[ ] “Minimum length of song” option
[ ] Think about what options to make mixable
[ ] Generate time intervals based off track length
[ ] Setup little mixer sort of thing w/ faders
[X] How does the stream end? What does that look like?

# Content

[X] What to call the Archive page
[X] Come up with some language for the home page @brian

# Bugs
[X] Prevent clicks outside of the button area once the buffer starts
    or cancel the buffer when clicking

# Social

[ ] Animate the loops
[ ] Send the next weeks loops

- 31x still image based on animation visual: Should include artist and track name and be sized for Twitter usage (1024x512px) - These don’t need to include a date
- 31x animated gif based on animation visual: My gut feeling would be to not include text on these… But this would only work if the visuals are strong enough on their own (so the overlay of aimation/press photos could work). Sized for Instagram usage (1080x1080px)
- 31x 30-second video based on animation visual and incorporating snippet of disintegrating track: Should include artist and track name but preferably rather small in a corner, so that it doesn’t infringe on the 20% text rule. Sized for Facebook usage (1600x900)
- 4x Generic key visual: Since we’re promoting this continually over the course of a month, it would probably be good to switch up what this looks like when we’re shouting about the project as a whole. I think these 4 could be a mix of static and animated gif versions (maybe 2 of each?)

# Notes

https://www.npmjs.com/package/sample-loader
and https://www.npmjs.com/package/sample-player
which work awesome, gonna try the timeout trick with these two also