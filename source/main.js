/**
 * Libraries
 */
require('velocity-animate')
require('velocity-ui-pack')

var fs = require('fs')
var path = require('path')
var http = require('http')
var moment = require('moment') 
var Router = require('./lib/router')
var rbma = require('./lib/rbma')

var _ = {
  assign: require('lodash/object/assign'),
  each: require('lodash/collection/each'),
  filter: require('lodash/collection/filter')
}

/**
 * Constructors
 */
var Splash = require('./splash')
var Strm = require('./strm')
var Calendar = require('./calendar')
var Audio = require('./audio/mixer')

/**
 * Data
 */
var data = {
  defaults: {
    duration: 1,
    interval: 'minutes'
  }
}

/**
 * CONFIG
 */
if (DIR === undefined) {
  DIR = '/specials/2016-shattered-streams'
}

/**
 * Router
 */
var router = new Router({
  always : function () {

  }
})

/**
 * Routes
 */
var splash
var calendar
var strm

/**
 * Resize
 */
function resize () {
  var size = 0

  if (window.innerHeight > window.innerWidth) {
    size = window.innerWidth
  } else {
    size = window.innerHeight
  }

  size = Math.floor(size * 0.045)
  document.querySelector('html').style.fontSize = size + 'px'
}

/*
 * Routes
 */
function routes () {
  router.on('/', {
    load: splash.load,
    unload: function () {
      strm.unload()
      splash.unload()
    }
  })

  router.on('/:stream', {
    load: splash.load,
    unload: function () {
      strm.unload()
      splash.unload()
    }
  })
  
  router.on('/calendar', {
    load: calendar.load,
    unload: calendar.unload
  })
  
  router.on('/calendar/:param', {
    load: calendar.load,
    unload: calendar.unload
  })

  router.on('/stream', {
    load: strm.load,
    unload: strm.unload
  })

  router.on('/stream/:param', {
    load: function (artist) {
      strm.load({
        artist: artist
      })
    },
    unload: strm.unload
  })
}

/**
 * Format Data
 */
function formatData () {
  var currentPath = window.location.hash.replace('#', '')
  var today = moment().format('YYYY-MM-DD')

  var activeTrack = _.filter(data.tracks, function(item, index) {
    return item.date === today
  })

  if (currentPath.indexOf('calendar') < 0 && currentPath.indexOf('stream') < 0) {
    activeTrack = _.filter(data.tracks, function(item, index) {
      return item.path.split(/-(.+)?/)[1] === currentPath.replace('/', '')
    })
  }

  if (activeTrack.length > 0) {
    activeTrack.active = true
    data.activetrack = activeTrack[0]
  } else {
    data.tracks[0].active = true
    data.activetrack = data.tracks[0]
  }

  _.each(data.tracks, function (track, i) {
    var dayDiff = moment().diff(track.date, 'days')

    if (dayDiff > 0) { 
      track.past = true
    } else if (dayDiff === 0 && 1/dayDiff === Infinity || track.active) {
      track.active = true
    } else {
      track.past = false
    }

    track.day = moment(track.date).format('DD')
  })
}

/**
 * Setup
 */
function setup () {
  /**
   * Splash
   */
  splash = Splash({
    el: document.querySelector('[data-splash]'),
    content: data
  }).setup()

  /**
   * Calendar
   */
  calendar = Calendar({
    el: document.querySelector('[data-calendar]'),
    content: data
  }).setup()

  /**
   * Stream
   */
  strm = Strm({
    content: data
  }).setup()

  /**
   * Events
   */
  splash.on('loaded', strm.load)
  strm.on('refresh', function () {
    router.go('/')
  })

  strm.on('end', strm.unload)
  strm.on('end', function () {
    router.go('/')
  })

  resize()
  
  window.addEventListener('resize', resize)
}

/**
 * Initialize
 */
http.get(DIR + '/data.json', function (res) {
  var _data = ''

  res.on('data', function (chunk) {
    _data += chunk
  })

  res.on('end', function () {
    data = _.assign(data, JSON.parse(_data))

    formatData()
    setup()
    routes()
  })
})