var audio_data = {
  vol: 0,
  low_vol: 0,
  high_vol: 0,

  low_hit: false,
  big_hit: false,
  high_hit: false,
}

function processAudio(){
  var low_threshold = .8;
  var overall_threshold = .8;
  var high_threshold = .5;

  var freq_domain = new Uint8Array(analyser.frequencyBinCount);

  analyser.getByteFrequencyData(freq_domain);
  var max_vol_lowpass = 0;
  var max_vol_highpass = 0;

  var max_vol = 0;

  for (var i = 0; i < analyser.frequencyBinCount; i++) {

    var value = freq_domain[i];
    var percent = value / 255;

    // assuming 44100hz
    // maximum amplitude for frequency < 80hz
    if ( i < analyser.frequencyBinCount * 0.0018){
      max_vol_lowpass = Math.max(percent, max_vol_lowpass);
    }

    // maximum amplitude for frequency > 16000hz
    if ( i > analyser.frequencyBinCount * .36 ){
      max_vol_highpass = Math.max(percent, max_vol_highpass);
    }

    // max amplitude regardless of frequency
    max_vol = Math.max(percent, max_vol);
  }

  audio_data.low_hit = ( max_vol_lowpass > low_threshold ) ? true : false;
  audio_data.high_hit = ( max_vol_highpass > high_threshold ) ? true : false;
  audio_data.big_hit = ( max_vol > overall_threshold) ? true : false;

  audio_data.vol = max_vol;
  audio_data.low_vol = max_vol_lowpass;
  audio_data.high_vol = max_vol_highpass;


  if ( audio_data.low_hit ){
    document.getElementById("low_hit").innerHTML = "low hit"

  } else {
    document.getElementById("low_hit").innerHTML = ""
  }

  if ( audio_data.high_hit ){
    document.getElementById("high_hit").innerHTML = "high hit"
  } else {
    document.getElementById("high_hit").innerHTML = ""
  }

  if ( audio_data.big_hit ){
    document.getElementById("big_hit").innerHTML = "vol hit"
  } else {
    document.getElementById("big_hit").innerHTML = ""
  }

}


var draw_frame;
function draw(){

      draw_frame = window.requestAnimationFrame(function(){
        draw();
      });

    processAudio();
}

var context = new (window.AudioContext || window.webkitAudioContext)();
var analyser = context.createAnalyser();


var audio = new Audio()
var buffer = context.createMediaElementSource(audio)

audio.src = 'ring_piece.mp3'
audio.controls = true
audio.autoplay = true

buffer.connect(analyser);
analyser.connect(context.destination);

document.addEventListener("DOMContentLoaded", function(event) {
  document.body.appendChild(audio);

  // doin stuff
  draw();
});





