var _ = {
  assign: require('lodash/object/assign')
}

// impulse responses by Fokke van Saane (http://fokkie.home.xs4all.nl/IR.htm)
var Overdrive = require('./overdrive')
var Fader = require('./fader')

/**
 * Audio
 */
function Audio (opts) {
  var audio = { }

  var context = new AudioContext()
  // var audioElement = document.getElementById('player')
  var carrier = false
  // var carrier = context.createMediaElementSource(audioElement)
  var convolver = context.createConvolver()
  var reverb = context.createConvolver()

  var dry = context.createGain()
  var wet = context.createGain()
  var noise = context.createGain()

  var overdrive = new Overdrive(context, {
    preBand: 1.0,
    color: 1500,
    drive: 0.1,
    postCut: 8000
  })

  var biquadFilter = context.createBiquadFilter()

  biquadFilter.type = 'lowshelf';
  biquadFilter.frequency.value = 500;
  biquadFilter.gain.value = 10;

  var bufferSize = 4096;
  var pinkNoise = (function() {
      var b0, b1, b2, b3, b4, b5, b6;
      b0 = b1 = b2 = b3 = b4 = b5 = b6 = 0.0;
      var node = context.createScriptProcessor(bufferSize, 1, 1);
      node.onaudioprocess = function(e) {
          var output = e.outputBuffer.getChannelData(0);
          for (var i = 0; i < bufferSize; i++) {
              var white = Math.random() * 2 - 1;
              b0 = 0.99886 * b0 + white * 0.0555179;
              b1 = 0.99332 * b1 + white * 0.0750759;
              b2 = 0.96900 * b2 + white * 0.1538520;
              b3 = 0.86650 * b3 + white * 0.3104856;
              b4 = 0.55000 * b4 + white * 0.5329522;
              b5 = -0.7616 * b5 - white * 0.0168980;
              output[i] = b0 + b1 + b2 + b3 + b4 + b5 + b6 + white * 0.5362;
              output[i] *= 0.11; // (roughly) compensate for gain
              b6 = white * 0.115926;
          }
      }
      return node;
  })();

  // pinkNoise.connect(noise)
  // noise.connect(biquadFilter)

  // CORS
  // audioElement.crossOrigin = 'anonymous'

  var connect = function() {
    // Hook everything up
    carrier.connect(overdrive.input)
    overdrive.connect(reverb)
    reverb.connect(biquadFilter)
    biquadFilter.connect(convolver)
    convolver.connect(wet)
    carrier.connect(dry)

    // Input and output
    dry.connect(context.destination)
    wet.connect(context.destination)
  }

  var mix = function (t) {
    // Signal mix
  	dry.gain.value = 1.0 - (t*t*t)
  	wet.gain.value = t*t*t*t*t * 1.5

    // FX modulation
    noise.gain.value = 0
    // noise.gain.value = t /2
    overdrive.drive = t / 4
    biquadFilter.frequency.value = 1000 - (t * 500)
  }

  var loadImpulse = function (fileName) {
    var url = fileName
    var request = new XMLHttpRequest()

    request.open('get', url, true)
    request.responseType = 'arraybuffer'

    request.onload = function () {
      context.decodeAudioData(request.response, function (buffer) {
        convolver.buffer = buffer
      }, function (e) {
        console.log(e)
      })
    }

    request.onerror = function (e) {
      console.log(e)
    }

    request.send()
  }

  var loadReverb = function (fileName) {
    var url = fileName
    var request = new XMLHttpRequest()

    request.open('get', url, true)
    request.responseType = 'arraybuffer'

    request.onload = function () {
      context.decodeAudioData(request.response, function (buffer) {
        reverb.buffer = buffer
      }, function (e) {
        console.log(e)
      })
    }

    request.onerror = function (e) {
      console.log(e)
    }

    request.send()
  }

  loadImpulse('assets/telephone.wav')
  loadReverb('assets/church.mp3')
  mix(0)

  document.getElementById('mix').addEventListener('input', function() {
    var value = this.value / 100
    var scale = function (t) { return 1-(--t)*t*t*t }
    mix(scale(value), 'mix')
  }, false)

  setupFaders()

  /**
   * Setup Faders
   */
  function setupFaders () {
    var faderOptions = {
      container: document.querySelector('[data-mixer]'),
      rangeClass: 'small-range',
      containerClass: ['x', 'c12']
    }

    var test = Fader(faderOptions)
    test.setup()
  }

  /**
   * On
   */
  audio.on = function on () {

    return audio
  }

  /**
   * Pause
   */
  audio.pause = function pause () {
    audioElement.pause()
    return audio
  }

  /**
   * Play
   */
  audio.play = function play () {
    audioElement.play()
    return audio
  }

  /**
   * Load Buffer
   */
  audio.loadBuffer = function loadBuffer (e) {
    var data = e.target.result
    context.decodeAudioData(data, function(buffer) {
      audio.playBuffer(buffer)
    })
  }

  /**
   * Play Buffer
   */
  audio.playBuffer = function playBuffer (buffer) {
    if (carrier) {
      carrier.stop()
    }
    
    carrier = context.createBufferSource()
    carrier.buffer = buffer
    connect()
    carrier.start(0)
  }

  /**
   * Load
   */
  audio.load = function play (url, autoplay) {
    // audioElement.setAttribute('src', '')
    // audio.pause()

    // audioElement.setAttribute('src', url)
    // if (autoplay) audio.play()

    // SC.get('/resolve/?url=https://soundcloud.com' + url, function (data) {
    //   if (data.streamable) {
    //     audioElement.setAttribute('src', data.stream_url + '?client_id=a61d18b64ed8378f4c3ef55a8fc3023a')
    //     if (autoplay) audio.play()
    //   } else {
    //     alert('Unable to load :(')
    //     audioElement.pause()
    //   }
    // })

    return audio
  }

  return audio
}

module.exports = Audio
