var _ = {
  each: require('lodash/collection/each'),
  assign: require('lodash/object/assign')
}

/**
 * Fader
 */
function Fader (opts) {
  var fader = { }

  /**
   * Options
   */
  var options = _.assign({
    container: document.body,
    containerClass: null,
    rangeClass: null,
    min: 0,
    max: 100
  }, opts)

  /**
   * Data
   */
  var data = {

  }

  /**
   * Element
   */
  var el = {
    container: createContainer(),
    range: createRange()
  }

  /**
   * Create Container
   */
  function createContainer () {
    var _el = document.createElement('div')
    if (options.containerClass) {
      _el.classList.add(options.containerClass)
    }
    return _el
  }

  /**
   * Create Range
   */
  function createRange () {
    var _el = document.createElement('input')
    _el.setAttribute('type', 'range')
    _el.setAttribute('min', options.min)
    _el.setAttribute('max', options.max)
    if (options.rangeClass) {
      _el.classList.add(options.rangeClass)
    }
    return _el
  }

  /**
   * Setup
   */
  fader.setup = function setup (opts) {
    el.container.appendChild(el.range)
    options.container.appendChild(el.container)
  }

  return fader
}

module.exports = Fader