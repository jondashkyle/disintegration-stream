var Emitter = require('tiny-emitter')

var _ = {
  assign: require('lodash/object/assign')
}

var createAudioContext = require('ios-safe-audio-context')
var Loader = require('sample-loader')
var Player = require('sample-player')
var Overdrive = require('./overdrive')
var Fader = require('./fader')

/**
 * Audio
 */
function Audio (opts) {
  var audio = { }
  var emitter = new Emitter()

  /**
   * Options
   */
  var options = _.assign({
    track: null
  }, opts)

  /**
   * Data
   */
  var data =  {
    timeline_position: 0,
    counter: 0,
    playing: false,
    tick: null,
    initialized: false,
    location: 'http://' + window.location.host + DIR,
    analyser: {
      vol: 0,
      low_vol: 0,
      high_vol: 0,

      low_hit: false,
      big_hit: false,
      high_hit: false
    },
    _mobileEnabled: false
  }

  /**
   * Context and instances
   */
  var context
  var loader
  var audioBuffer

  var carrier
  var analyser
  var convolver
  var reverb

  /**
   * Signals
   */
  var dry
  var wet_1
  var wet_2

  /**
   * Filters
   */
  var overdrive
  var biquadFilter
  var notchFilter

  /**
   * Connect
   */ 
  function connect() {
    // carrier >                                               > dry
    // carrier > overdrive > biquad > convolver                > wet_1 
    //                              > reverb > notchFilter  > wet_2

    carrier.output.connect(dry) // -> to dry 
    carrier.output.connect(analyser) // -> to analyzer
    carrier.output.connect(overdrive.input)

    overdrive.connect(biquadFilter)
    biquadFilter.connect(convolver)
    convolver.connect(wet_1) // -> to wet_1

    biquadFilter.connect(reverb)
    reverb.connect(notchFilter)
    notchFilter.connect(wet_2) // -> to wet_2
    
    dry.connect(context.destination)
    wet_1.connect(context.destination)
    wet_2.connect(context.destination)
  }

  /**
   * Mix
   */
  function mix (t) {
    // Signal mix

    // var gain1 = 1 - t;
    // var gain2 = t;

    var gain1, gain2, gain3,

    gain1 = Math.max(Math.cos(t * Math.PI), 0)
    gain2 = Math.max(Math.cos(t * Math.PI+ -.5*Math.PI), 0)
    gain3 = Math.max(Math.cos(t * Math.PI+ -1*Math.PI), 0)

    // var gain1 = Math.cos(t * 0.5*Math.PI);
    // var gain2 = Math.cos((1.0 - t) * 0.5*Math.PI);
    // dry.gain.value = gain1;
    // wet.gain.value = gain2;

// <<<<<<< ours
    // pre_convolution_dry.gain.value = gain1;
    // pre_convolution_wet.gain.value = gain2;
// =======
    // console.log(Math.floor(gain1*100), Math.floor(gain2*100), t);

    dry.gain.value = gain1;
    wet_1.gain.value = gain2;
    wet_2.gain.value = gain3;

    // convolved.gain.value = Math.max(t-.25, 0) * 10;
    // dry.gain.value = 1.0 - (t*t*t)
    // wet.gain.value = t*t*t*t*t * 2

    // FX modulation
    // overdrive.color = 2000 - (t * 1800)

    biquadFilter.frequency.value = 8000 - (t * 4000)

    var vol = Math.max((data.analyser.vol-.5)*2, 0);
    var attentuation = Math.random() < (t*.12) ? -250: -20;

    vol = Math.min(vol * vol * vol *3, 1)
    // vol = Math.max((vol-.75)*4, 0)
    if (data.counter % 2 == 0){
      notchFilter.gain.value = attentuation;
    }

    notchFilter.frequency.value = 1500 - (t * 700);
    notchFilter.Q.value = 1000;
    
    overdrive.drive = t * t * 2
    overdrive.color = (t * vol) * 1500 + ((1-t) *2000);
    // console.log(notchFilter.Q.value)
    
    // console.log(vol)
  }

  /**
   * Process Audio
   */
  function processAudio() {

    var freq_domain = new Uint8Array(analyser.frequencyBinCount)
    analyser.getByteFrequencyData(freq_domain)

    var max_vol_lowpass = 0
    var max_vol_highpass = 0
    var max_vol = 0
    var ambient_vol = 0;


    for (var i = 0; i < analyser.frequencyBinCount; i++) {

      var value = freq_domain[i]
      var percent = value / 255;

      ambient_vol+=percent;

      // assuming 44100hz
      // maximum amplitude for frequency < 80hz
      // if ( i < analyser.frequencyBinCount * ((biquadFilter.frequency.value+80) / 44100) ) {
      if ( i < analyser.frequencyBinCount * .018) {
        max_vol_lowpass = Math.max(percent, max_vol_lowpass)
      }

      // maximum amplitude for frequency > 16000hz
      if ( i > analyser.frequencyBinCount * .45 ) {
        max_vol_highpass = Math.max(percent, max_vol_highpass)
      }

      // max amplitude regardless of frequency
      max_vol = Math.max(percent, max_vol)
    }
    ambient_vol = (ambient_vol/(i+1));
    
    // console.log(Math.floor(max_vol_highpass*100), Math.floor(ambient_vol*100))

    data.analyser.low_hit = ( (max_vol_lowpass > ambient_vol*3.5) && max_vol_lowpass > max_vol *.99) ? true : false
    data.analyser.high_hit = ( (max_vol_highpass > ambient_vol*1.2) && max_vol_highpass > max_vol *.2) ? true : false

    data.analyser.vol = max_vol
    data.analyser.low_vol = max_vol_lowpass
    data.analyser.high_vol = max_vol_highpass

    if ( data.analyser.low_hit ) { emitter.emit('low')}
    if ( data.analyser.high_hit ) { emitter.emit('high') }
  }

  /**
   * Load Impulse
   */
  function loadImpulse (fileName) {
    var url = fileName
    var request = new XMLHttpRequest()

    request.open('get', url, true)
    request.responseType = 'arraybuffer'

    request.onload = function () {
      context.decodeAudioData(request.response, function (buffer) {
        convolver.buffer = buffer
      }, function (e) {
        console.log(e)
      })
    }

    request.onerror = function (e) {
      console.log(e)
    }

    request.send()
  }

  /**
   * Load Reverb
   */
  function loadReverb (fileName) {
    var url = fileName
    var request = new XMLHttpRequest()

    request.open('get', url, true)
    request.responseType = 'arraybuffer'

    request.onload = function () {
      context.decodeAudioData(request.response, function (buffer) {
        reverb.buffer = buffer
      }, function (e) {
        console.log(e)
      })
    }

    request.onerror = function (e) {
      console.log(e)
    }

    request.send()
  }

  /**
   * Tick
   */
  function tick() {
    data.tick = window.requestAnimationFrame(tick)
    data.counter++;
    processAudio()
  }

  /**
   * Debug
   */
  function debug () {
    var mixer = document.createElement('input')
    var adjustMix = function () {
      var value = this.value / 100
      var scale = function (t) { return 1-(--t)*t*t*t }
      mix(value, 'mix');
      // mix(scale(value), 'mix')
    }

    mixer.setAttribute('max', '100')
    mixer.setAttribute('type', 'range')
    document.body.appendChild(mixer)

    mixer.addEventListener('input', adjustMix, false)
    mixer.addEventListener('change', adjustMix, false)

    mixer.style.position = 'fixed'
    mixer.style.top = '50px'
    mixer.style.left = '50px'
    mixer.style.display = 'block'
    mixer.style.zIndex = 999
  }

  audio.progress = function(t){
    data.timeline_position = t.position;
    mix(data.timeline_position);    
  }

  /**
   * Setup
   */
  audio.setup = function setup () {
    if (data.initialized) {
      return audio
    } else {
      data.initialized = true

      /**
       * Context and instances
       */
      context = createAudioContext()
      loader = Loader(context)

      carrier = Player(context.destination, {
        gain: 1,
        loop: true
      })

      analyser = context.createAnalyser()
      convolver = context.createConvolver()
      convolver.normalize = true;
      reverb = context.createConvolver()
      reverb.normalize = true;

      /**
       * Signals
       */
      dry = context.createGain()
      wet_1 = context.createGain()
      wet_2 = context.createGain()

      /**
       * Overdrive
       */
      overdrive = new Overdrive(context, {
        preBand: 1.0,
        color: 1500,
        drive: 0.1,
        postCut: 8000
      })

      /**
       * Filter
       */
      biquadFilter = context.createBiquadFilter()
      biquadFilter.type = 'lowpass';
      biquadFilter.frequency.value = 500;
      biquadFilter.Q.value = 2;

      /**
       * Notch Filter
       */
      notchFilter = context.createBiquadFilter()
      notchFilter.type = 'peaking';
      notchFilter.frequency.value = 5000;
      notchFilter.gain.value = -80;
      notchFilter.Q.value = 100;
    }

    if (options.track) {
      loader(DIR + '/tracks/' + options.track.path + '/' + options.track.track).then(function (buffer) {
        audioBuffer = buffer
      })
    }

    loadImpulse(data.location + '/assets/telephone.wav')
    loadReverb(data.location + '/assets/noise_fade_down_5.wav')

    connect()
    mix(0)
    // debug()

    return audio
  }

  /**
   * On
   */
  audio.on = function on (ev, cb) {
    emitter.on(ev, cb)
    return audio
  }

  /**
   * Off
   */
  audio.off = function off (ev, cb) {
    emitter.removeListener(ev, cb)
    return audio
    window.cancelAnimationFrame(data.tick);
  }

  /**
   * Stop
   */
  audio.stop = function stop () {
    if (data.playing) {
      carrier.stop()
      data.playing = false
    }
    window.cancelAnimationFrame(data.tick);


    return audio
  }

  /**
   * Play
   */
  audio.play = function play () {
    if (! data.playing) {
      carrier.start(audioBuffer, context.currentTime)
      tick()
      data.playing = true
    }
    return audio
  }

  /**
   * Load
   */


  /**
   * Load
   */
  audio.load = function play (trackPath) {
    if (trackPath) {
      loader(data.location + '/tracks/' + trackPath + '/' + options.track.track).then(function (buffer) {
        audioBuffer = buffer
      })
    }
    
    return audio
  }

  return audio
}

module.exports = Audio
