var fs = require('fs')
var path = require('path')
var ejs = require('ejs')
var moment = require('moment')
var Emitter = require('tiny-emitter')

var _ = {
  each: require('lodash/collection/each'),
  assign: require('lodash/object/assign'),
  filter: require('lodash/collection/filter')
}

var Audio = require('../audio')
var Artwork = require('../artwork')
var Playbar = require('../playbar')

var template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8')

/**
 * Stream
 */
function Stream (opts) {
  var strm = { }
  var emitter = new Emitter()

  /**
   * Options
   */
  var options = _.assign({
    el: document.querySelector('[data-strm]')
  }, opts)

  /**
   * Data
   */
  var data = {
    initialized: false,
    loop: null,
    time: {
      beginning: 0,
      end: 0,
      diff: 0
    },
    mobileReady: false,
    mobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
  }

  /**
   * Defaults
   */
  var defaults = {

  }

  /**
   * Artwork
   */
  var artwork

  /**
   * Playbar
   */
  var playbar 

  /**
   * Audio
   */
  var audio

  /**
   * Progress
   */
  function progress () {
    var now = moment()
    var diff = now.diff(data.time.beginning, defaults.interval, true)
    var position = (diff / defaults.duration) * 1

    if (diff >= defaults.duration) {
      emitter.emit('end')
    } else {
      emitter.emit('tick', { position: position })
      data.loop = window.requestAnimationFrame(progress)
    }
  }

  /**
   * Show
   */
  function show () {
    options.el.style.display = 'block'
  }

  /**
   * Hide
   */
  function hide () {
    options.el.style.display = 'none'
  }

  /**
   * Reset
   */
  function reset () {

  }

  /**
   * Setup
   */
  function setup () {
    if (options.el) {
      options.el.style.display = 'none'
      options.el.innerHTML = ejs.render(template, options.content)
    }

    if (data.initialized) {
      return
    } else {
      data.initialized = true
    }

    artwork = Artwork({
        el: options.el.querySelector('[data-artwork]'),
        track: options.content.activetrack
      })

    audio = Audio({
        track: options.content.activetrack
      })
      .on('low', artwork.hitLow)
      .on('high', artwork.hitHigh)

    if (! data.mobile) {
      audio.setup()
    } else {
      document.body.addEventListener('touchend', audio.setup)
    }

    playbar = Playbar({
        el: options.el.querySelector('[data-playbar]'),
        track: options.content.activetrack
      })
      .setup()

    options.el.querySelector('[data-refresh]')
      .addEventListener('click', function () {
        strm.unload()
        emitter.emit('refresh')
      }, false)

    emitter.on('tick', playbar.progress)
    emitter.on('tick', artwork.progress)
    emitter.on('tick', audio.progress)
  }

  /**
   * Setup
   */
  strm.setup = function () {
    setTimeout(setup, 500)
    return strm
  }

  /**
   * Audio Setup
   */
  strm.audioSetup = function audioSetup () {
    audio
      .setup()
      .on('low', artwork.hitLow)
      .on('high', artwork.hitHigh)
      .on('vol', artwork.hitVol)
    return strm
  }

  /**
   * On
   */
  strm.on = function on (ev, cb) {
    emitter.on(ev, cb)
    return strm
  }

  /**
   * Off
   */
  strm.off = function off (ev, cb) {
    emitter.removeListener(ev, cb)
    return strm
  }

  /**
   * Load
   */
  strm.load = function load (def) {
    if (!data.initialized) {
      setTimeout(strm.load, 100)
      return strm
    }

    defaults = _.assign(options.content.defaults, def)
    data.time.beginning = moment()
    data.time.end = moment(data.time.beginning).add(defaults.duration, defaults.interval)
    data.time.diff = data.time.end.diff(data.time.beginning, defaults.interval)

    audio
      .load()
      .play()

    artwork.load()
    playbar.start()

    show()
    progress()

    return strm
  }

  /**
   * Unload
   */
  strm.unload = function unload () {
    hide()
    audio.stop()
    artwork.unload()
    playbar.stop()

    window.cancelAnimationFrame(data.loop)

    return strm
  }

  return strm
}

module.exports = Stream
