var fs = require('fs')
var path = require('path')
var ejs = require('ejs')
var nut = require('nut')
var mbl = require('mbl')
var marked = require('marked')

var _ = {
  each: require('lodash/collection/each'),
  assign: require('lodash/object/assign'),
  merge: require('lodash/object/merge')
}

var template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8')

/**
 * Videos
 */
function Videos (opts) {
  var self = { }

  /**
   * Options
   */
  var options = _.assign({
    el: document.querySelector('[data-videos]')
  }, opts)

  /**
   * Data
   */
  var data = {
    initialized: false,
    playing: false
  }


  /**
   * Setup
   */
  self.setup = function setup () {
    if (data.initialized) {
      return self
    } else {
      data.initialized = true
    }

    if (options.el) {
      options.el.style.display = 'none'
      options.el.innerHTML = options.el.innerHTML = ejs.render(template, options.content)
    }

    return self
  }

  /**
   * Load
   */
  self.load = function load () {
    options.el.style.display = 'block'

    nut('.thumb-container').forEach(function(thumb) {
      var artist = nut('.thumb-artist', thumb)
      var frames = nut('.thumb-frames-track .data-thumb', thumb)

      var animateTrack = function () {
        thumb.querySelector('.thumb-frames-track').classList.add('thumb-frames-animate')
      }

      var loadFrames = function () {
        mbl(frames, {
          sequential: true,
          mode: 'background',
          complete: animateTrack
        }).start()
      }

      if (artist.length > 0) {
        mbl(artist, {
          sequential: true,
          mode: 'background',
          complete: loadFrames
        }).start()
      }
    })

    return self
  }

  /**
   * Unload
   */
  self.unload = function unload () {
    options.el.style.display = 'none'
    return self
  }

  return self
}

module.exports = Videos
