var _ = {
  each: require('lodash/collection/each'),
  assign: require('lodash/object/assign')
}

/**
 * Artwork
 */
function Artwork (opts) {
  var artwork = { }

  /**
   * Options
   */
  var options = _.assign({
    el: document.body
  }, opts)

  /**
  * mobile check
  **/
window.mobilecheck = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
}  


  /**
   * Data
   */
  var data = {
    mobile: mobilecheck(),
    multiplier: mobilecheck() == true? 0.5 : 1,
    running: false,
    severity: 0,
    exp_severity: 0,
    distortion_mode: "",
    num_frames: 0,
    location: 'http://' + window.location.host + DIR,

    distort_direction: {
      target_x: 0,
      target_y: 0,
      x: 0,
      y: 0
    },

    static_locations: [
      new PIXI.Point(0, 0),
      new PIXI.Point(-512, 0),
      new PIXI.Point(-512, -512),
      new PIXI.Point(0, -512)
    ],

    audio: {
      high: false,
      vol: false,
      low: false,
      high_lead: false,
      vol_lead: false,
      low_lead: false,

      queue: {
        low: [],
        high: [],
        vol: []
      }
    },

    needs_update: false,
    transform_counter: 0,
    counter: 0,
    draw_counter: 0,
    deform_counter: 0,
    frame_counter: 0,

    displace_offset: {
      x: 0,
      y: 0
    },

    cursor_target: {
      x:0,
      y:0
    },
    cursor: {
      dx: 0,
      dy: 0,
      x: 0,
      y: 0
    },
    delta: {
      x: 0,
      y: 0
    }
  }

  /**
  * Setup
  */

  var drawFrameID;

  /**
  *
  * This is the structure of the setup
  *
  TODO: logarithmic severity

  * renderer
  render_layer
    static_sprite
      texture: grain_1536.jpg
    distortion_layer
      filter: distortion
      composition_layers
       /stages
        filters:
          blur
          twist
          displacement
            texture: filter_NRM.jpg
            TODO: new distortion textures  
  *      /output_sprites
  *        /sprite_stages
  *          background_masks
  *            texture: black_mask.png
  *          sprites
  *            textures: renderTextures / 2 / currentTextures
  *
  * renderer renders each stage in rotation
  * every time a distortion happens, all rendertextures are updated at once
  * background_masks used to 'clear' the stage if it gets too white
  *
  **/

  var sprites = [];
  var output_sprites = [];
  var sprite_stages = [];
  var composition_layers = [];
  var render_layer = new PIXI.Container();
  var distortion_layer = new PIXI.Container();

  var stages = [];
  var renderTextures = [];
  var renderTextures2 = [];
  var currentTextures = [];
  var static_sprite = new PIXI.Sprite.fromImage(data.location + "/assets/grain_1536.jpg");

  static_sprite.blendMode = PIXI.BLEND_MODES.NORMAL;
  static_sprite.alpha = .2;

  var spray_sprite = new PIXI.Sprite.fromImage(data.location + "/assets/spray.png");
  var smear_sprite = new PIXI.Sprite.fromImage(data.location + "/assets/smear_up.jpg");

  var displacement_sprite = new PIXI.Sprite.fromImage(data.location + "/assets/filter_blur.jpg");
    // set image flag on sprite to signify its from filter_blur.jpg
    displacement_sprite.from_image = true;
    displacement_sprite.width = 1024*data.multiplier;

  var composite_displacement_sprite = new PIXI.Sprite.fromImage(data.location + "/assets/filter_blur.jpg");
  var background_masks = [];

  // I wanna get this from the json file
  var path_string = data.location + "/tracks/"+options.track.path + "/";
  var tex_urls = ["01.png","02.png","03.png","04.png","05.png","06.png"];
  var distortions = options.track.artwork.distortions;
  var beat_distortions = options.track.artwork.beat_distortions;


  // number of frames inside animation
  data.num_frames = tex_urls.length;

  var renderer = PIXI.autoDetectRenderer(1024*data.multiplier, 1024*data.multiplier, {resolution: data.multiplier});
  renderer.view.style.position = "fixed";
  renderer.view.style.top = "-100%";
  renderer.view.style.left = "-100%";
  renderer.view.style.right = "-100%";
  renderer.view.style.bottom = "-100%";
  renderer.view.style.margin = "auto auto auto"
  renderer.view.style.width = "100.5vmax";
  renderer.view.style.height = "100.5vmax";

  renderer.backgroundColor = 0x000000;

  for (var i=0; i < data.num_frames; i++) {

    stages[i] = new PIXI.Container();
    composition_layers[i] = new PIXI.Container();

    composition_layers[i].zIndex = i;    

    // create two render textures... these dynamic textures will be used to draw the scene into itself
    if ( data.multiplier < 1){
      renderTextures[i] = new PIXI.RenderTexture(renderer, renderer.width*2, renderer.height*2);
      renderTextures2[i] = new PIXI.RenderTexture(renderer, renderer.width*2, renderer.height*2);
    } else {
      renderTextures[i] = new PIXI.RenderTexture(renderer, renderer.width, renderer.height);
      renderTextures2[i] = new PIXI.RenderTexture(renderer, renderer.width, renderer.height);      
    }


    currentTextures[i] = renderTextures[i];

    displacement_sprite.height = 1024*data.multiplier;

    output_sprites[i] = new PIXI.Sprite(currentTextures[i]);
    output_sprites[i].pivot = {
      x: 512*data.multiplier,
      y: 512*data.multiplier
    }
    output_sprites[i].position = {
      x: 512*data.multiplier,
      y: 512*data.multiplier
    }

    stages[i].addChild(output_sprites[i]);

    background_masks[i] = new PIXI.Sprite.fromImage(data.location + "/assets/black_mask.png");
    background_masks[i].tint = 0x000000;
    background_masks[i].width = 1024*data.multiplier;
    background_masks[i].height = 1024*data.multiplier;
    background_masks[i].alpha = 0;

    sprite_stages[i] = new PIXI.Container();
    sprite_stages[i].addChild(background_masks[i]);

    sprites[i] = new PIXI.Sprite.fromImage(path_string+tex_urls[i]);
    sprites[i].filters = [new PIXI.filters.InvertFilter()];
    sprites[i].filters[0].invert = 0;

    // anchor sprite in middle of window
    sprites[i].anchor = {
      x: 0.5,
      y: 0.5
    };

    sprites[i].position = {
      x: 512*data.multiplier,
      y: 512*data.multiplier
    };

    sprite_stages[i].addChild(sprites[i]);

    stages[i].filters = [
      new PIXI.filters.BlurFilter(),
      new PIXI.filters.TwistFilter(),
      new PIXI.filters.DisplacementFilter(displacement_sprite),
      new PIXI.filters.DisplacementFilter(spray_sprite),
      new PIXI.filters.DisplacementFilter(smear_sprite)
    ];

    // init blur to 0
    stages[i].filters[0].blur = 0;

    // init twist to 0
    stages[i].filters[1].radius = 0;
    stages[i].filters[1].angle = 0;

    // init displacement to 0
    stages[i].filters[2].scale = {x: 0, y: 0};
    stages[i].filters[3].scale = {x: 0, y: 0};

    sprite_stages[i].position = {
      x: 0,
      y: 0
    }

    stages[i].addChild(sprite_stages[i]);
    composition_layers[i].addChild(stages[i]);
    distortion_layer.addChild(composition_layers[i]);
  }

  distortion_layer.filters = [new PIXI.filters.DisplacementFilter(composite_displacement_sprite)];
  distortion_layer.filters[0].scale = {x: 0, y: 0};

  render_layer.addChild(distortion_layer);
  render_layer.addChild(static_sprite);

  render_layer.addChild(displacement_sprite);
  render_layer.addChild(composite_displacement_sprite);
  render_layer.addChild(spray_sprite);
  render_layer.addChild(smear_sprite);


  render_layer.filters = [ new PIXI.filters.ColorMatrixFilter(), new PIXI.filters.BloomFilter()];
  render_layer.filters[0].contrast(1.5, true);

  /**
  * pause draw loop
  **/

  function pause() {    
    window.cancelAnimationFrame(drawFrameID);
    data.running = false;
  }



  /**
   * Draw loop
   */

  function draw(){
    var temp;

    drawFrameID = window.requestAnimationFrame(function(){
      draw();
    });
    data.running = true;



    for (var i=0; i < data.num_frames; i++) {

      if ( data.draw_counter < data.num_frames){

        // seed the feedback loop with the images
        sprites[i].visible = true;
        data.needs_update = true;

      } else {

      // after we've seeded the image feedback loop, we deactivate the source images
        sprites[i].visible = false;
        sprite_stages[i].tint = 0xffffff;
      }

      // render textures on every frame

    }


    // % 4 = on twos
    // % 2 = on ones
    // % 1 = 60 fps
    // draw_counter used for any animation effects
    // if counter is used then things have tendency to lock in groove




    if ( data.counter % (4*data.multiplier) == 0){

      if ( Math.abs(data.cursor.dx) > 1.5 || Math.abs(data.cursor.dy) > 1.5){

          data.distortion_mode = "feederbacker";
          data.needs_update = true;

      } else {

        // detect low & high beats
        detectHits();
        data.needs_update = false;

        // if we have a hit, use it, with a 20% chance of failing
        if ( (data.audio.low_lead || data.audio.high_lead) && Math.random() < .8){

          // always transform when recording hit
          data.transform_counter++;

          // if low but not high
          if ( data.audio.low_lead && !data.audio.high_lead ){

            data.distortion_mode = beat_distortions[ (0+data.transform_counter*2) % beat_distortions.length ];
            data.needs_update = true;

          // if high but not low
          } else if ( !data.audio.low_lead && data.audio.high_lead ){

            data.distortion_mode = beat_distortions[ (1+data.transform_counter*2) % beat_distortions.length ];
            data.needs_update = true;

          }

        // if no beats or beats fail, we have a chance of choosing first option
        } else if ( Math.random() > .3 + (1-data.severity)*.8 ){

          data.distortion_mode = distortions[ (0+data.transform_counter*4) % distortions.length ];
          data.needs_update = true;

        } else {
          // 40% chance of choosing second option
          if ( Math.random() > .5 ){

            data.distortion_mode = distortions[ (1+data.transform_counter*4)  % distortions.length ];
            data.needs_update = true;

          // 10% chance of choosing third option
          } else if ( Math.random() > .5 ){

            data.distortion_mode = distortions[ (2+data.transform_counter*4)  % distortions.length ];
            data.needs_update = true;

          // 5% chance of choosing fourth option
          } else if ( Math.random() > .5) {

            data.distortion_mode = distortions[ (3+data.transform_counter*4)  % distortions.length ]
            data.needs_update = true;

          // 5% chance of nothing happening
          } else {

            data.transform_counter++;
            data.needs_update = false;

          }

        }

        // override inversions with exposure for first few frames
        if ( data.draw_counter < 200 && (data.distortion_mode == "invert" || data.distortion_mode == "invertBlue" ) ){
          data.distortion_mode = "expose";
          data.needs_update = true;
        }

        //override rotate with rotateminor for first third
        if ( data.distortion_mode =="rotate" && data.severity < .3){
          data.distortion_mode = "rotateMinor";
          data.needs_update = true;          
        }


      }


      // console.log(data.distortion_mode, data.needs_update);

      if (data.needs_update){

        animateSprite();
        data.needs_update = false;
        data.distortion_mode = "";
      }      

      data.draw_counter ++;
      data.frame_counter = data.draw_counter % data.num_frames;

    }

         

    // Choose new direction for the major distortion overlay to go to
    if (data.counter % 32 == 0){

      data.distort_direction.target_x = (Math.random()-.5) *10;
      data.distort_direction.target_y = (Math.random()-.5) *10;

    }
        

  

    // here's where to do by-frame stuff

    data.cursor.dx = (data.cursor.x *.95 + data.cursor_target.x*.05)- data.cursor.x;
    data.cursor.dy = (data.cursor.y *.95 + data.cursor_target.y*.05)- data.cursor.y;

    data.cursor.x = data.cursor.x *.95 + data.cursor_target.x*.05;
    data.cursor.y = data.cursor.y *.95 + data.cursor_target.y*.05;

    for (var i=0; i < data.num_frames; i++) {
      composition_layers[i].visible = false;

    }
    composition_layers[data.frame_counter].visible = true;

    distortion_layer.filters[0].scale = {
      x: 50*data.exp_severity,
      y: 50*data.exp_severity
    }

    composite_displacement_sprite.x = data.cursor.x*.2 + data.displace_offset.x;
    composite_displacement_sprite.y = data.cursor.y*.2 + data.displace_offset.y;
    composite_displacement_sprite.width = 1024*data.multiplier;
    composite_displacement_sprite.height = 1024*data.multiplier;

    data.distort_direction.x = .995*data.distort_direction.x+.005*data.distort_direction.target_x;
    data.distort_direction.y = .995*data.distort_direction.y+.005*data.distort_direction.target_y;

    data.displace_offset.x += data.distort_direction.x;
    data.displace_offset.y += data.distort_direction.y;   

    data.displace_offset.x = data.displace_offset.x % 1024*data.multiplier;
    data.displace_offset.y = data.displace_offset.y % 1024*data.multiplier;

    // composition_layers[0].visible = true;
    // console.log(data.frame_counter, (data.frame_counter+1)%4, (data.counter%4)/3);

    render_layer.filters[1].blur = data.severity*4;


    static_sprite.position = data.static_locations[data.counter %4];
    static_sprite.position.x = Math.max(-512, Math.min(0, static_sprite.position.x+(Math.random()-.5)*100))
    static_sprite.position.y = Math.max(-512, Math.min(0, static_sprite.position.x+(Math.random()-.5)*100))

    renderer.render(render_layer);

    data.counter++;

  }


  // here's where we detect beats
  function detectHits(){

    var low_hit = false,
      high_hit = false;

    for ( var i = 0; i < data.audio.queue.low.length; i++ ){
      if (data.audio.queue.low[i] == true){
        low_hit = true;
      }
    }    

    for ( var i = 0; i < data.audio.queue.high.length; i++ ){
      if (data.audio.queue.high[i] == true){
        high_hit = true;
      }
    }

    data.audio.low_lead = !data.audio.low && low_hit ? true : false;
    data.audio.high_lead = !data.audio.high && high_hit ? true : false;


    data.audio.low = low_hit;
    data.audio.high = high_hit;

    data.audio.queue.high = [false]
    data.audio.queue.low = [false] 

  }


  /**
  * Animation router
  **/
  function animateSprite(){

    // needs_update is a flag when we distort the frames & need to re-render
      // console.log('needs update', data.distortion_mode);

      switch(data.distortion_mode) {

        case "expose":
          expose();
          break;

        case "invert":
          invert();
          break;

        case "invertBlue":
          invertBlue();
          break;

        case "displace":
          displace();
          break;

        case "rotateMinor":
          rotateMinor();
          break;          

        case "rotate":
          rotate();
          break;

        case "twirl":
          twirl();
          break;

        case "blur":
          blur();
          break;

        case "spray":
          spray();
          break;

        case "smear":
          smear();
          break;          

        case "zoomIn":
          zoomIn();
          break;

        case "zoomOut":
          zoomOut();
          break;

        case "feederbacker":
          feederbacker();
          break;

        default:
          break;
      }


      // loop through the frames and re-render again,
      // reset the mode so that the feedback doesnt run out of control
      updateRenderTextures();
      reset();

  }

  function feederbacker(){



    // smear_sprite.x = Math.floor(data.deform_counter*.2*data.exp_severity) %512;
    // smear_sprite.y = Math.floor(data.deform_counter*.2*data.exp_severity) %512;

    // smear_sprite.height = (1000+ (Math.random()-.5)*400) * (1-data.severity)  + (data.severity) * (2048 * Math.random() + 20);
    // smear_sprite.width = (1000+ (Math.random()-.5)*400) * (1-data.severity)  + (data.severity) * (2048 * Math.random() + 20);

    for (var i=0; i < data.num_frames; i++){



      sprites[i].tint = "0x0000ff";

      sprites[i].alpha = 0.15 * data.severity;
      sprites[i].visible = true;   

      stages[i].filters[2].scale = { 
        x: data.cursor.dx*5 * ( data.exp_severity+.05*data.severity),
        y: data.cursor.dy*5 * ( data.exp_severity+.05*data.severity)
      };



    }

  }

  /**
  * Function to update renderTextures on each stage
  **/

  function updateRenderTextures(){
    var temp;
    for (var i=0; i < data.num_frames; i++) {


        temp = renderTextures[i];
        renderTextures[i] = renderTextures2[i];
        renderTextures2[i] = temp;

        output_sprites[i].texture = renderTextures[i];
        renderTextures2[i].render(stages[i]);

    }
  }

  /**
  * Possible distortions to be made to each stage
  **/

  // Adds dark version of animation to stage
  function invert(){

    var x_offset = Math.random()-.5;
    var y_offset = Math.random()-.5;

    for (var i=0; i < data.num_frames; i++){


      sprites[i].tint = 0x000000;
      sprites[i].alpha = 0.8*data.severity;
      sprites[i].visible = true;      

      // also, offset... gettin wild
      sprites[i].position = {
        x: 512*data.multiplier + x_offset *400*data.severity*data.multiplier,
        y: 512*data.multiplier + y_offset *400*data.severity*data.multiplier
      }

      sprites[i].width = 1024*data.multiplier + Math.abs(x_offset *400*data.severity*2)*data.multiplier;
      sprites[i].height = 1024*data.multiplier + Math.abs(x_offset *400*data.severity*2)*data.multiplier;
      // sprites[i].width = 

      // TODO : enlarge sprite to avoid seeing edge after offset

    }

  }

  // Adds blue version of animation to stage
  function invertBlue(){

    var x_offset = Math.random()-.5;
    var y_offset = Math.random()-.5;

    for (var i=0; i < data.num_frames; i++){


      sprites[i].tint = 0x0000ff;
      sprites[i].alpha = 0.3;
      sprites[i].visible = true;      

      // also, offset... gettin wild
      sprites[i].position = {
        x: 512*data.multiplier + x_offset *400*data.severity*data.multiplier,
        y: 512*data.multiplier + y_offset *400*data.severity*data.multiplier
      }

      var biggest_offset = Math.max(Math.abs(sprites[i].position.x-(512*data.multiplier)), Math.abs(sprites[i].position.y-(512*data.multiplier)));

      sprites[i].width = biggest_offset*2 + (data.multiplier*1024);
      sprites[i].height = sprites[i].width;

    }

  }  

  // re-exposes original animation to stage
  function expose(){

    for (var i=0; i < data.num_frames; i++){

      // at very beginning exposing will nearly reset the animation
      // as time goes on it's purely additive
      background_masks[i].alpha = (1-data.severity)*.05+.015;

      sprites[i].filters[0].invert = 0;
      sprites[i].tint = 0xffffff;
      sprites[i].alpha = .4;
      sprites[i].visible = true;
      displace();
    }

  }

  // sprays noise upward
  function smear(){

    smear_sprite.width = 1024*data.multiplier;
    smear_sprite.height = 1024*data.multiplier;
    // smear_sprite.x = Math.floor(data.deform_counter*.2*data.exp_severity) %512;
    // smear_sprite.y = Math.floor(data.deform_counter*.2*data.exp_severity) %512;

    // smear_sprite.height = (1000+ (Math.random()-.5)*400) * (1-data.severity)  + (data.severity) * (2048 * Math.random() + 20);
    // smear_sprite.width = (1000+ (Math.random()-.5)*400) * (1-data.severity)  + (data.severity) * (2048 * Math.random() + 20);

    for (var i=0; i < data.num_frames; i++){

      stages[i].filters[4].scale = { 
        x: Math.cos(data.deform_counter * .2 +  data.severity )*(40*data.exp_severity) + 1,
        y: Math.sin(data.deform_counter * .2 +  data.severity )*(100*data.exp_severity)
        
      };

    }

    data.deform_counter++;
 

  }  

  // sprays noise everywhere
  function spray(){

    spray_sprite.width = 1024*data.multiplier;
    spray_sprite.height = 1024*data.multiplier;
    // spray_sprite.x = Math.floor(data.deform_counter*.2*data.exp_severity) %512;
    // spray_sprite.y = Math.floor(data.deform_counter*.2*data.exp_severity) %512;

    // spray_sprite.height = (1000+ (Math.random()-.5)*400) * (1-data.severity)  + (data.severity) * (2048 * Math.random() + 20);
    // spray_sprite.width = (1000+ (Math.random()-.5)*400) * (1-data.severity)  + (data.severity) * (2048 * Math.random() + 20);

    for (var i=0; i < data.num_frames; i++){

      stages[i].filters[3].scale = {
        x: Math.sin(data.deform_counter * .2 +  data.severity )*(200*data.exp_severity),
        y: Math.cos(data.deform_counter * .2 +  data.severity )*(200*data.exp_severity)
      };

    }

    data.deform_counter++;


  }

  // Rotates animation
  function rotate(){
    var rotate_seed = Math.sin(data.deform_counter+Math.random()*2);

    for (var i=0; i < data.num_frames; i++){

      output_sprites[i].rotation = Math.PI*data.deform_counter*.5+ rotate_seed*.3*data.exp_severity;
    }
    data.deform_counter++;
  }

  function rotateMinor(){
    var rotate_seed = Math.sin(data.deform_counter+Math.random());


    for (var i=0; i < data.num_frames; i++){

      output_sprites[i].rotation+= rotate_seed *.4*(data.exp_severity + .2);
    }

    data.deform_counter++;

  }

  // applies twirl effect
  function twirl(){
    var twirl_seed = Math.sin(data.deform_counter+Math.random()*4);

    for (var i=0; i < data.num_frames; i++){
      stages[i].filters[1].offset = {
        x: 0.5 + Math.sin(twirl_seed)*data.severity*.1,
        y: 0.5 + Math.cos(twirl_seed+1)*data.severity*.1
      }
      stages[i].filters[1].radius = 1;
      stages[i].filters[1].angle = Math.sin(data.deform_counter*.02)*.2*data.exp_severity;
    }
    data.deform_counter++;

  }

  // applies blur / duplication
  function blur(){

    for (var i=0; i < data.num_frames; i++){
      stages[i].filters[0].blur = Math.sin(data.counter*i*Math.PI*3 +data.deform_counter)*data.severity*10;
    }

    data.deform_counter++;

  }



  // adds shrunken image
  function zoomOut(){

    var zoom_offset = -35*(data.exp_severity);
    var new_position = zoom_offset * -.5 + 512*data.multiplier;

    for (var i=0; i < data.num_frames; i++){

      output_sprites[i].width = 1024*data.multiplier+zoom_offset*data.multiplier;
      output_sprites[i].height = 1024*data.multiplier+zoom_offset*data.multiplier;
      output_sprites[i].position = {
        x: 512*data.multiplier,
        y: 512*data.multiplier
      }
    }

  }

  // adds enlarged image
  function zoomIn(){

    var zoom_offset = 20*data.exp_severity;

    for (var i=0; i < data.num_frames; i++){
      output_sprites[i].width = 1024*data.multiplier+zoom_offset*data.multiplier;
      output_sprites[i].height = 1024*data.multiplier+zoom_offset*data.multiplier;
      output_sprites[i].position = {
        x: 512*data.multiplier,
        y: 512*data.multiplier
      }
      // output_sprites[i].position = {x: new_position+512, y: new_position+512};
    }

  }

  // runs frames through displacement filter
  function displace(){

    displacement_sprite.x = Math.floor(data.deform_counter*.2*data.exp_severity) %512*data.multiplier;
    displacement_sprite.y = Math.floor(data.deform_counter*.2*data.exp_severity) %512*data.multiplier;

    displacement_sprite.height = (1000+ (Math.random()-.5)*400) * (1-data.severity)  + (data.severity) * (2048 * Math.random() + 20);
    displacement_sprite.width = (1000+ (Math.random()-.5)*400) * (1-data.severity)  + (data.severity) * (2048 * Math.random() + 20);

    for (var i=0; i < data.num_frames; i++){

      stages[i].filters[2].scale = {
        x: Math.sin(data.deform_counter * .2 +  data.severity )*(5+100*data.exp_severity),
        y: Math.cos(data.deform_counter * .2 +  data.severity )*(5+100*data.exp_severity)
      };

    }

    data.deform_counter++;

  }

  // resets all filters and modifications to default
  function reset(){

    for (var i=0; i < data.num_frames; i++){
      background_masks[i].alpha = 0;

      smear_sprite.rotation = 0;
      spray_sprite.rotation = 0;


      sprites[i].filters[0].invert = 0;
      output_sprites[i].width = 1024*data.multiplier;
      output_sprites[i].height = 1024*data.multiplier;
      output_sprites[i].position = {x: 512*data.multiplier, y:512*data.multiplier};
      output_sprites[i].rotation = 0;

      stages[i].filters[0].blur = 0;

      stages[i].filters[2].scale = {x: 0, y: 0};
      stages[i].filters.x = 0;
      stages[i].filters.y = 0;


      stages[i].filters[3].scale  = {
        x: 0,
        y: 0
      }
      stages[i].filters[4].scale = {
        x: 0,
        y: 0
      }

      stages[i].filters[1].radius = 0;
      stages[i].filters[1].angle = 0;



      sprites[i].visible = false;
      sprites[i].width = 1024*data.multiplier;
      sprites[i].height = 1024*data.multiplier;
      sprites[i].position = {
        x: 512*data.multiplier,
        y: 512*data.multiplier
      }

    }
  }

  // re-exposes original animation to stage
  function completeReset(){

    for (var i=0; i < data.num_frames; i++){

      background_masks[i].alpha = 1;

      sprites[i].filters[0].invert = 0;
      sprites[i].tint = 0xffffff;
      sprites[i].alpha = 1;
      sprites[i].visible = true;

    }

    updateRenderTextures();

  }    


  /**
   * Resize
   */
  function resize () {
    // currently CSS-based
    // changing renderer dimensions midstream would freak things up

    // options.el.offsetWidth;
    // options.el.offsetHeight
  }


  /**
   * Mousemove
   */
  function mousemove (event) {

    if ( data.mobile ){

      var x = event.touches[0].clientX * -1
      var y =event.touches[0].clientY

    } else {

      var x = event.clientX -1;
      var y = event.clientY;
    }

    var x = event.clientX || event.touches[0].clientX * -1
    var y = event.clientY || event.touches[0].clientY

    var dx = data.cursor.x - x
    var dy = data.cursor.y - y

    // if (Math.abs(dx) > Math.abs(data.delta.x) && Math.abs(dx) < 50 && data.cursor.x !== 0) {
      // data.delta.x = data.cursor.x - x
    // }

    // if (Math.abs(dy) > Math.abs(data.delta.y) && Math.abs(dy) < 50 && data.cursor.y !== 0) {
      // data.delta.y = data.cursor.y - y
    // }

    data.cursor_target.x = x;
    data.cursor_target.y = y;

    // data.cursor.x = x
    // data.cursor.y = y
  }

  artwork.hitLow = function(){
    data.audio.queue.low.push(true);
    // console.log("low");
  }

  artwork.hitHigh = function(){
    data.audio.queue.high.push(true);
    // console.log("high");

  }

  artwork.progress = function(t){
    data.severity = t.position;
    data.exp_severity = data.severity * data.severity;

    // console.log(t.position);
  }

  /**
   * On
   */
  artwork.load = function load () {
    data.counter = 0;
    data.draw_counter = 0;

    completeReset();
    reset();
// savin cycles
    draw();
    window.addEventListener('mousemove', mousemove, false)
    window.addEventListener('touchmove', mousemove, false)

    // window.addEventListener('resize', resize, false)
    return artwork
  }

  /**
   * Off
   */
  artwork.unload = function unload () {
    pause();
    window.removeEventListener('mousemove', mousemove, false)
      window.removeEventListener('touchmove', mousemove, false)
    // window.removeEventListener('resize', resize, false)
    return artwork
  }

  /**
   * Initialize
   */
  options.el.appendChild(renderer.view)
  resize();
  // draw();

  return artwork
}

module.exports = Artwork
