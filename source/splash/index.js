var fs = require('fs')
var path = require('path')
var nut = require('nut')
var ejs = require('ejs')
var Emitter = require('tiny-emitter')


var _ = {
  each: require('lodash/collection/each'),
  assign: require('lodash/object/assign')
}

var template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8')

/**
 * Splash
 */
function Splash (opts) {
  var splash = { }
  var emitter = new Emitter()

  /**
   * Options
   */
  var options = _.assign({
    el: document.querySelector('[data-splash]')
  }, opts)

  /**
   * Data
   */
  var data = {
    initialized: false,
    active: false,
    mobileReady: false,
    buttonsReady: false
  }

  /**
   * Highlight Options
   */
  function highlightOptions () {
    var blueEls = Array.apply(null, options.el.querySelectorAll('label[data-reveal]'))
    
    if (options.el.querySelector('input:checked')) {
      return
    }

    blueEls.forEach(function(blueEl, i) {
      Velocity(blueEl, {
        color: ['#000', '#00f']
      }, {
        delay: 250 * (i+1),
        duration: 500,
        easing: [1],
        complete: function(_el) {
          _el[0].style.color = ''
        }
      })
    })
  }

  /**
   * Touch End
   */
  function touchEnded () {
    if (! data.mobileReady) {
      data.mobileReady = true
      emitter.emit('touchend')
    }
  }

  /**
   * Show
   */
  function show () {
    var revealEls = Array.apply(null, options.el.querySelectorAll('[data-reveal]'))

    revealEls.forEach(function(revealEl) {
      var delay = parseInt(revealEl.getAttribute('data-reveal')) - 1

      var revealSequence = [
        { e: revealEl, p: { opacity: 1 }, o: { duration: Math.random() * 100, easing: [1], delay: 500 * delay} },
        { e: revealEl, p: { opacity: 0 }, o: { duration: Math.random() * 100, easing: [1] } },
        { e: revealEl, p: { opacity: 1 }, o: { duration: Math.random() * 100, easing: [1] } },
        { e: revealEl, p: { opacity: 0 }, o: { duration: Math.random() * 100, easing: [1] } },
        { e: revealEl, p: { opacity: 1 }, o: { duration: Math.random() * 100, easing: [1] } },
        { e: revealEl, p: { opacity: 0 }, o: { duration: Math.random() * 100, easing: [1] } },
        { e: revealEl, p: { opacity: 1 }, o: { duration: Math.random() * 100, easing: [1] } }
      ]

      Velocity.RunSequence(revealSequence)
    })

    // setTimeout(highlightOptions, 2000)

    setTimeout(function () {
      options.el.style.display = 'block'
    }, 5)
  }

  /**
   * Hide
   */
  function hide () {
    
  }

  /**
   * Setup
   */
  splash.setup = function setup () {
    options.el.style.display = 'none'
    options.el.innerHTML = ejs.render(template, options.content)
    data.initialized = true

    var buttons = options.el.querySelectorAll('input[type="radio"]')

    for (var i = 0; i < buttons.length; i++) {
      var elId = buttons[i].getAttribute('id')
      var elLabel = options.el.querySelector('label[for="' + elId + '"]')
      buttons[i].addEventListener('click', buttonClicked, false)
      elLabel.addEventListener('touchend', touchEnded, false)
    }

    return splash
  }

  /**
   * Button Click
   */
  function buttonClicked (e) {
    var duration = options.el.querySelector('input[name="duration"]:checked')
    var interval = options.el.querySelector('input[name="interval"]:checked')
    var hoursEl = options.el.querySelector('input[name="interval"][value="hours"]')
    var intervalsEl = options.el.querySelector('[data-intervals]')

    if (duration) {
      if (parseInt(duration.value) > 24) {
        if (hoursEl.checked === true) {
          options.el.querySelector('input[name="interval"][value="minutes"]').checked = true
          hoursEl.checked = false
        }
        hoursEl.disabled = true
      } else {
        hoursEl.disabled = false
      }
    }

    if (duration && !interval) {
      intervalsEl.classList.add('button-blink')
    }

    if (duration && interval) {
      intervalsEl.classList.remove('button-blink')

      // Transition in if we haven't already
      if (! data.buttonsReady) {
        Velocity(options.el.querySelector('[data-buttons]'), {
          translateY: '-3rem'
        }, {
          duration: 500,
          easing: 'easeOutQuint'
        })
      }

      // Trigger the load
      Velocity(options.el.querySelector('[data-progress]'), 'stop')
      Velocity(options.el.querySelector('[data-progress]'), {
        translateX: ['0%', '-100%']
      }, {
        duration: 4000,
        complete: loaded
      })

      data.buttonsReady = true
    } else {
      return
    }
  }

  /**
   * Loaded
   */
  function loaded () {
    options.el.querySelector('[data-splash-border]').style.display = 'none'
    deconstruct()

    emitter.emit('loaded', {
      duration: parseInt(options.el.querySelector('input[name="duration"]:checked').value),
      interval: options.el.querySelector('input[name="interval"]:checked').value
    })
  }

  /**
   * Deconstruct
   */
  function deconstruct () {
    var deconstuct = options.el.querySelectorAll('[data-deconstruct]')
    var finishedCount = 0

    for (var i = 0; i < deconstuct.length; i++) {
      var viewportOffset = deconstuct[i].getBoundingClientRect()
      var translateX = window.innerWidth * -1

      if (viewportOffset.left >= window.innerWidth / 2) {
        translateX = window.innerWidth
      }

      Velocity(deconstuct[i], {
        translateY: (Math.random () * (window.innerHeight*2)) - window.innerHeight,
        translateX: translateX,
        rotateZ: (Math.random() * 180) - 90 + 'deg',
        rotateX: (Math.random() * 180) - 90 + 'deg',
        rotateY: (Math.random() * 180) - 90 + 'deg'
      }, {
        duration: (Math.random () * 1000) + 1000,
        easing: 'easeInQuint',
        complete: function () {
          finishedCount += 1
          if (finishedCount === deconstuct.length) {
            splash.unload()
          }
        }
      })
    }
  }

  /**
   * Reconstruct
   */
  function reconstruct () {
    var buttonsEl = options.el.querySelector('[data-buttons]')
    var revealEls = Array.apply(null, document.querySelectorAll('[data-reveal]'))
    var deconstuctEls = Array.apply(null, document.querySelectorAll('[data-deconstruct]'))
    
    data.buttonsReady = false

    Velocity(revealEls, 'stop')
    revealEls.forEach(function (revealEl) {
      revealEl.style.opacity = 0
    })

    nut('input', options.el).forEach(function (buttonEl) {
      buttonEl.checked = false
    })

    Velocity(deconstuctEls, 'stop')
    Velocity(deconstuctEls, {
      translateY: 0,
      translateX: 0,
      rotateZ: 0,
      rotateX: 0,
      rotateY: 0
    }, {
      duration: 1
    })

    Velocity(buttonsEl, 'stop')
    Velocity(buttonsEl, {
      translateY: '0'
    }, {
      duration: 1
    })

    options.el.querySelector('[data-intervals]').classList.remove('button-blink')

  }

  /**
   * On
   */
  splash.on = function on (ev, cb) {
    emitter.on(ev, cb)
    return splash
  }

  /**
   * Off
   */
  splash.off = function off (ev, cb) {
    emitter.removeListener(ev, cb)
    return splash
  }

  /**
   * Load
   */
  splash.load = function load () {
    if (! data.active) {
      data.active = true
      show()
    }
    return splash
  }

  /**
   * Unload
   */
  splash.unload = function unload () {
    data.active = false

    Velocity(options.el.querySelector('[data-progress]'), 'stop')
    options.el.querySelector('[data-progress]').setAttribute('style', '')
    options.el.querySelector('[data-splash-border]').style.display = ''
    options.el.style.display = 'none'

    reconstruct()

    return splash
  }

  return splash
}

module.exports = Splash
