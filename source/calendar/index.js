var fs = require('fs')
var path = require('path')
var ejs = require('ejs')
var nut = require('nut')
var mbl = require('mbl')
var marked = require('marked')
var Emitter = require('tiny-emitter')

var _ = {
  each: require('lodash/collection/each'),
  assign: require('lodash/object/assign'),
  merge: require('lodash/object/merge')
}

var template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8')

/**
 * Calendar
 */
function Calendar (opts) {
  var self = { }
  var emitter = new Emitter()

  /**
   * Options
   */
  var options = _.assign({
    el: document.querySelector('[data-calendar]')
  }, opts)

  /**
   * Data
   */
  var data = {
    initialized: false
  }

  /**
   * Flip
   */
  function flip () {
    var deg = [180 * -1, 180]
    var degRan = deg[Math.floor(Math.random() * deg.length)]
    this.querySelector('.thumb').style.transform = 'rotateY(' + degRan + 'deg)'
  }

  /**
   * Flip Reset
   */
  function flipReset () {
    this.querySelector('.thumb').setAttribute('style', '')
  }

  /**
   * Flip Toggle
   */
  function flipToggle () {

  }

  /**
   * Setup
   */
  self.setup = function setup () {
    if (data.initialized) {
      return self
    } else {
      data.initialized = true
    }

    if (options.el) {
      options.el.innerHTML = ejs.render(template, _.merge(options.content, { marked: marked }))
      options.el.style.display = 'none'
    }
    
    return self
  }

  /**
   * On
   */
  self.on = function on (ev, cb) {
    emitter.on(ev, cb)
    return self
  }

  /**
   * Off
   */
  self.off = function off (ev, cb) {
    emitter.removeListener(ev, cb)
    return self
  }

  /**
   * Load
   */
  self.load = function load (route) {
    options.el.style.display = 'block'

    if (route === 'all') {
      options.el.innerHTML = ejs.render(template, _.merge(options.content, { showAll: true, marked: marked }))
    }

    nut('.thumb-container').forEach(function(thumb) {
      var artist = nut('.thumb-artist', thumb)
      var frames = nut('.thumb-frames-track .data-thumb', thumb)

      var animateTrack = function () {
        thumb.querySelector('.thumb-frames-track').classList.add('thumb-frames-animate')
      }

      var loadFrames = function () {
        mbl(frames, {
          sequential: true,
          mode: 'background',
          complete: animateTrack
        }).start()
      }

      if (artist.length > 0) {
        mbl(artist, {
          sequential: true,
          mode: 'background',
          complete: loadFrames
        }).start()
      }

      thumb.addEventListener('mouseenter', flip)
      thumb.addEventListener('mouseleave', flipReset)
      thumb.addEventListener('click', flipToggle)
    })

    return self
  }

  /**
   * Unload
   */
  self.unload = function unload () {
    options.el.style.display = 'none'
    return self
  }

  return self
}

module.exports = Calendar
