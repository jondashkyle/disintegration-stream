(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-21347469-1', 'auto');
ga('send', 'pageview');

window.edWrapperNameSpace = { }

window.edWrapperNameSpace.aGTM = [
"GTM-LVJ5",     // hq
"GTM-7W7M",     // hq
"GTM-HPZK",     // hq
"GTM-WWXSFT"    // property
];

var edWrapper = [];

window.edWrapperNameSpace.sVers = -1;
window.edWrapperNameSpace.aInitialInnerWrapper = [["downloads.redbull.com/webtrekk/innereWrapper/gtm/innerWrapper_gtm.js",false]];
window.edWrapperNameSpace.sWrapperSrc = ["downloads.redbull.com/webtrekk/edAnalyticsWrapper.js","downloads.redbull.com/webtrekk/confWrapper.js"];

(function (sVarName) {
  window.edWrapperNameSpace.sEdWrapperObjectName = sVarName; 
  window[sVarName] = window[sVarName] || [];
  var script = document.createElement('script'); script.type = 'text/javascript';script.asyn = false;
  script.src = ('https:' == document.location.protocol ? 'https://' : 'http://')+window.edWrapperNameSpace.sWrapperSrc[0] + "?v=" + window.edWrapperNameSpace.sVers;
  var script2 = document.getElementsByTagName('script')[0]; script2.parentNode.insertBefore(script, script2);
})("edWrapper");

edWrapper.push( [ 'trackPageView' , false , {1:'IntMS' , 2:'IntMS - int' , 3:'IntMS - int - Red Bull Music Academy'} , true ] );
edWrapper.push( [ 'trackPageVar' , 'Language' , 'en'] );
edWrapper.push( [ 'trackPageVar' , 'Domain incl. Subdomain' , window.location.hostname] );
edWrapper.push( [ 'trackPageVar' , 'Type' , 'IntMS'] );
edWrapper.push(['submit']);