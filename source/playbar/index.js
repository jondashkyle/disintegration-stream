var fs = require('fs')
var path = require('path')
var ejs = require('ejs')
var moment = require('moment')

var _ = {
  each: require('lodash/collection/each'),
  assign: require('lodash/object/assign')
}

var template = fs.readFileSync(path.join(__dirname, 'template.html'), 'utf8')

/**
 * Playbar
 */
function Playbar (opts) {
  var playbar = { }

  /**
   * Options
   */
  var options = _.assign({
    el: document.querySelector('[data-playbar]')
  }, opts)

  /**
   * Data
   */
  var data = {
    initialized: false,
    playing: false
  }

  /**
   * Show
   */
  function show () {
    options.el.style.opacity = 1
    Velocity(options.el, {
      translateY: ['0%', '100%']
    }, {
      display: 'block',
      duration: 1500,
      easing: 'easeOutQuint'
    })
  }

  /**
   * Setup
   */
  playbar.setup = function setup () {
    if (data.initialized) {
      return playbar
    } else {
      data.initialized = true
    }

    if (options.el) {
      options.el.innerHTML = options.el.innerHTML = ejs.render(template, options.track)
      options.positionEl = options.el.querySelector('[data-playbar-position]')
    }
    
    return playbar
  }

  /**
   * Progress
   */
  playbar.progress = function progress (_data) {
    options.positionEl.style.transform = 'translateX(' + ((_data.position * 100) - 100) + '%)'
  }

  /**
   * Start
   */
  playbar.start = function start (duration) {
    if (data.playing) {
      return playbar
    }

    options.positionEl.style.display = 'block'
    setTimeout(show, 1000)

    return playbar
  }

  /**
   * Stop
   */
  playbar.stop = function stop () {
    options.positionEl.style.display = 'none'
    options.el.style.opacity = 0
    return playbar
  }

  return playbar
}

module.exports = Playbar
